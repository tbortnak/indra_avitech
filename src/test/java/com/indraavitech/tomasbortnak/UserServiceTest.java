package com.indraavitech.tomasbortnak;

import com.indraavitech.tomasbortnak.model.entities.UserEntity;
import com.indraavitech.tomasbortnak.model.repositories.UserRepository;
import com.indraavitech.tomasbortnak.services.UserService;
import org.junit.Assert;
import org.junit.Test;

import javax.transaction.Transactional;
import java.util.List;

public class UserServiceTest {

    UserService userService = new UserService();

    UserRepository userRepository = new UserRepository();

    @Test
    @Transactional
    public void add(){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setGuid("a1");
        userEntity.setName("Robert");

        userService.add(1L, "a1", "Robert");

        List<UserEntity> users = userRepository.findAll();

        Assert.assertEquals(userEntity.getName(), users.get(0).getName());
    }
}
