package com.indraavitech.tomasbortnak.services;

import com.indraavitech.tomasbortnak.model.entities.UserEntity;
import com.indraavitech.tomasbortnak.model.repositories.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserService {

    UserRepository userRepository = new UserRepository();

    public void add(Long userId, String userGuid, String username){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setGuid(userGuid);
        userEntity.setName(username);
        userRepository.save(userEntity);
    }

    public void printAll(){
        List<UserEntity> usersEntities = userRepository.findAll();
        if (usersEntities.isEmpty()){
            System.out.println("There are no users in database.");
        }else usersEntities.stream().forEach(userEntity -> printUser(userEntity));
    }

    public void deleteAll(){
        List<UserEntity> usersEntities = userRepository.findAll();
        usersEntities.stream().forEach(userEntity -> userRepository.delete(userEntity));
    }

    public void commit(){
        userRepository.commit();
    }

    private void printUser(UserEntity userEntity){
        System.out.println("==================");
        System.out.println("USER_ID : " + userEntity.getId());
        System.out.println("USER_GUID : " + userEntity.getGuid());
        System.out.println("USER_NAME : " + userEntity.getName());
    }
}
