package com.indraavitech.tomasbortnak.services;

import com.indraavitech.tomasbortnak.model.entities.UserEntity;

import java.util.concurrent.ExecutorService;

class Produce implements Runnable {
    private final ExecutorService consumer;

    public Produce(ExecutorService consumer) {
        this.consumer = consumer;
    }

    @Override
    public void run(){
            UserService userService = new UserService();
            userService.add(1L, "a1", "Robert");
            userService.add(2L, "a2", "Martin");
            userService.commit();
            Runnable consume = new Consume(userService);
            consumer.submit(consume);
    }
}
