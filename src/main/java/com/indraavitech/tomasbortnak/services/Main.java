package com.indraavitech.tomasbortnak.services;

import java.sql.SQLException;
import java.util.concurrent.*;


public class Main {

    public static void main(String[] args) throws SQLException {

        ExecutorService consumer = new ThreadPoolExecutor(1,4,30, TimeUnit.SECONDS,new LinkedBlockingQueue<Runnable>(100));

        ExecutorService producer = Executors.newSingleThreadExecutor();

        Runnable produce = new Produce(consumer);
        producer.submit(produce);
    }
}
