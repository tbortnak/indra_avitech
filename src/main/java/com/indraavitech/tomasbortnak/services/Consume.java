package com.indraavitech.tomasbortnak.services;

import com.indraavitech.tomasbortnak.model.entities.UserEntity;

class Consume implements Runnable {
    private final UserService userService;

    public Consume(UserService userService){
        this.userService = userService;
    }

    @Override
    public void run() {
        userService.printAll();
        userService.deleteAll();
        userService.printAll();
    }
}
