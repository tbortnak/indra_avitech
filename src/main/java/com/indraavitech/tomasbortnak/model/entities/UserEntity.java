package com.indraavitech.tomasbortnak.model.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "SUSERS")
public class UserEntity {

    @Id
    @Column(name = "USER_ID", nullable = false)
    private Long id;

    @Column(name = "USER_GUID", nullable = false)
    private String guid;

    @Column(name = "USER_NAME", nullable = false)
    private String name;
}
