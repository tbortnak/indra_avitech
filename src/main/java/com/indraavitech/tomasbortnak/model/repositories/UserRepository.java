package com.indraavitech.tomasbortnak.model.repositories;

import com.indraavitech.tomasbortnak.model.entities.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserRepository {

    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    Session session = sessionFactory.openSession();

    Transaction transaction = session.beginTransaction();

    public void save(UserEntity userEntity){
        session.save(userEntity);
    }

    public List<UserEntity> findAll(){
        return session.createCriteria(UserEntity.class).list();
    }

    public void commit(){
        transaction.commit();
    }

    public void delete(UserEntity userEntity){
        session.delete(userEntity);
        session.flush();
    }

}
